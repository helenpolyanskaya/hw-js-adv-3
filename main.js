// Деструктурізація - це переписиванія массива чи обʼєкта таким чином, що їм присваівається відразу декілка переміниї (частинам массиу.обʼєкта), які дають можливисть напряму звертатися до їх властивостей, даних.


///// 1)

const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

const puk = new Set();

for (item of clients1){
  puk.add(item)
}

for (item of clients2){
  puk.add(item)
}


console.log("Завдання №1:")
console.log(puk)



// 2)

const characters = [
    {
      name: "Елена",
      lastName: "Гилберт",
      age: 17, 
      gender: "woman",
      status: "human"
    },
    {
      name: "Кэролайн",
      lastName: "Форбс",
      age: 17,
      gender: "woman",
      status: "human"
    },
    {
      name: "Аларик",
      lastName: "Зальцман",
      age: 31,
      gender: "man",
      status: "human"
    },
    {
      name: "Дэймон",
      lastName: "Сальваторе",
      age: 156,
      gender: "man",
      status: "vampire"
    },
    {
      name: "Ребекка",
      lastName: "Майклсон",
      age: 1089,
      gender: "woman",
      status: "vempire"
    },
    {
      name: "Клаус",
      lastName: "Майклсон",
      age: 1093,
      gender: "man",
      status: "vampire"
    }
  ];

//let charactersShortInfo = [];

function processCharacter(elem) {
  let {name, lastName, age} = elem;
  return {name: name, lastName: lastName, age: age};
}

const charactersShortInfo = characters.map(processCharacter)

//const charactersShortInfo = characters.map(({name, lastName, age}) => ({name: name, lastName: lastName, age: age}) )

// for ( let elem of characters) 
// {
//     let {name, lastName, age} = elem;
//     charactersShortInfo = [...charactersShortInfo, {name: name, lastName: lastName, age: age}]
// }

console.log("Завдання №2:")
console.log(charactersShortInfo)

///// 3)

const user1 = {
    name: "John",
    years: 30
};

let { name: imya, years: vik, isAdmin=false} = user1;

console.log("Завдання №3:")
console.log(imya);
console.log(vik);
console.log(isAdmin);

///// 4) 

const satoshi2020 = {
    name: 'Nick',
    surname: 'Sabo',
    age: 51,
    country: 'Japan',
    birth: '1979-08-21',
    location: {
      lat: 38.869422, 
      lng: 139.876632
    }
}
  
const satoshi2019 = {
    name: 'Dorian',
    surname: 'Nakamoto',
    age: 44,
    hidden: true,
    country: 'USA',
    wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
    browser: 'Chrome'
}
  
const satoshi2018 = {
    name: 'Satoshi',
    surname: 'Nakamoto', 
    technology: 'Bitcoin',
    country: 'Japan',
    browser: 'Tor',
    birth: '1975-04-05'
}

let satoshi2023 = {...satoshi2018, ...satoshi2019, ...satoshi2020};
console.log("Завдання №4:")
console.log(satoshi2023);

///// 5)

const books = [{
    name: 'Harry Potter',
    author: 'J.K. Rowling'
  }, {
    name: 'Lord of the rings',
    author: 'J.R.R. Tolkien'
  }, {
    name: 'The witcher',
    author: 'Andrzej Sapkowski'
  }];
  
  const bookToAdd = {
    name: 'Game of thrones',
    author: 'George R. R. Martin'
  }

let booksNew = [...books, bookToAdd];
console.log("Завдання №5:")
console.log(booksNew);
  
///// 6) 

const employee = {
    name: 'Vitalii',
    surname: 'Klichko'
}

let employee2 = {...employee, age: 51, salary: '53400 UAH'};

console.log("Завдання №6:")
console.log(employee2);

///// 7)

const array = ['value', () => 'showValue'];

// Допишіть код тут
let [ value, showValue] = array

alert(value); // має бути виведено 'value'
alert(showValue());  // має бути виведено 'showValue'


